# Javascript Coding Standards Draft


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Javascript Coding Standards Draft](#javascript-coding-standards-draft)
  - [Types](#types)
  - [References](#references)
  - [Objects](#objects)
  - [Arrays](#arrays)
  - [Destructuring](#destructuring)
  - [Strings](#strings)
  - [Functions](#functions)
  - [Arrow Functions](#arrow-functions)
  - [Classes and Constructors](#classes-and-constructors)
  - [Modules](#modules)
  - [Iterators and Generators](#iterators-and-generators)
  - [Properties](#properties)
  - [Variables](#variables)
  - [Hoisting](#hoisting)
  - [Comparison Operators & Equality](#comparison-operators--equality)
  - [Blocks](#blocks)
  - [Control Statements](#control-statements)
  - [Comments](#comments)
  - [Whitespace](#whitespace)
  - [Commas](#commas)
  - [Semicolons](#semicolons)
  - [Type Casting & Coercion](#type-casting--coercion)
  - [Naming Conventions](#naming-conventions)
  - [Accessors](#accessors)
  - [Events](#events)
  - [jQuery](#jquery)
  - [ECMAScript 5 Compatibility](#ecmascript-5-compatibility)
  - [Standard Library](#standard-library)

<!-- /code_chunk_output -->

## Types

- 1.1 **Primitives:*- When you access a primitive value work directly on the value.

```javascript
const foo = 1;
let bar = foo;

bar = 10;

console.log(foo, bar); // => 1, 10
```

- 1.2 **Complex:*- When  you access a complex type you work on a reference to its value.

```js
const foo = [1,2];
const bar = foo;

bar[0] = 9;

console.log(foo[0], bar[0]); // => 9, 9
```

## References

- 2.1 Use `const` for all of your references; avoid using `var`. eslint options: `prefer-const`, `no-const-assign`

> This ensures you can't reassign a reference, which can lead to bugs and harder to understand code

```js
// bad
var a = 1;

//good
const a = 1;
```

- 2.2 If you need to reassign a reference, use `let` instead of `var`. eslint: `no-var`

> This is due to `let` being block-scoped rather than function scoped like `var`.

```js
// bad
var count = 1;
if(true){
    count += 1;
}

// good
let count = 1;
if(true){
    count += 1;
}
```

- 2.3 It is worth noting that both `let` and `const` are block-scoped

```js
// const and let only exist in the blocks they are defined in.
{
    let a = 1;
    const  b = 1;
}

console.log(a) // ReferenceError
console.log(b) // ReferenceError
```

## Objects

- 3.1 Use the literal syntax for object creation. eslint: `no-new-object`

```js
// bad
const item = new Object();

// good
const item = {}
```

- 3.2 Use computed property names when creating objects with dynamic property names

> This allows you to define all properties of an object in the same place.

```js
function getKey(k) {
    return `a key named ${k}`;
}

// bad
const obj = {
    id: 5,
    name: 'Oxford',
};
obj[getKey('enabled')] = true;

// good
const obj = {
    id: 10,
    name: 'Truro',
    [getKey('enabled')]: true,
};
```

- 3.3 Use object method shorthand. eslint: `object-shorthand`

```js
// bad
const test = {
    value: 1,

    addValue: function(value) {
        return atom.value + value;
    },
};

// good
const test = {
    value: 1,

    addValue(value){
        return atom.value + value;
    },
};
```

- 3.4 Use property value shorthand. eslint: `object-shorthand`

> This is shorter to write and more descriptive.

```js
const nikiLauda = 'Niki Lauda';

// bad
const obj = {
    nikiLauda: nikiLauda,
};

// good
const obj = {
    nikiLauda,
};
```

- 3.5 Group shorthand properties together at the beginning of an object declaration.

> This makes it easier to tell which properties are shorthand.

```js
const tracer = 'Lena Oxton';
const mei = 'Mei-Ling Zhou';

// bad
const obj = {
    reinhardt: 'Reinhardt Wilhelm',
    tracer,
    dva: 'Hana Song',
    mei,
    reaper: 'Gabriel Reyes'
}

// good
const obj = {
    tracer,
    mei,
    reinhardt: 'Reinhardt Wilhelm',
    dva: 'Hana Song',
    reaper: 'Gabriel Reyes'
}
```

- 3.6 Only quote properties that are invalid identifiers. eslint: `quote-props`

> This is subjectively easier to read. It improves syntax highlighting and is more easily optimised by many JS engines.

```js
// bad
const bad = {
    'hello': 'there',
    'general': 'kenobi',
    'data-blah': 3,
};

// good
const good = {
    hello: 'there',
    general: 'kenobi',
    'data-blah': 3,
};
```

- 3.7 Do not call `Object.prototype` methods directly, such as `hasOwnProperty`, `propertyIsEnumerable` and `isPrototypeOf`.

> These methods may be shadowed by properties on the object in question - e.g. { hasOwnProperty: false } - or, the object may be a null object.

```js
// bad
console.log(object.hasOwnProperty(key));

// good
console.log(Object.prototype.hasOwnProperty.call(object, key));

// best
const has = Object.prototype.hasOwnProperty; // cache the lookup once, in module scope.

console.log(has.call(object, key));
```

- 3.8 Prefer the object spread operator over `Object.assign` to shallow-copy objects. Use the object rest operator to get a new object with certain properties omitted.

```js
// very bad
const original = { a: 1, b: 2 };
const copy = Object.assign(original, { c: 3 }); // this mutates 'original' no bueno.
delete copy.a; // As does this.

// bad but not as bad
const original = { a: 1, b: 2 };
const copy = Object.assign({}, original, { c: 3 }); // copy => { a: 1, b: 2, c: 3 }

// good
const original = { a: 1, b: 2 };
const copy = { ...original, c: 3 }; // copy => { a: 1, b: 2, c: 3 }

const { a, ...noA } = copy; // noA => { b: 2, c: 3 }
```

## Arrays

- 4.1 Use the literal syntax for array creation. eslint: `no-array-constructor`

```js
// bad
const items = new Array();

// good
const items = [];
```

- 4.2 Use `Array.push` instead of direct assignment to add items to an array

```js
const anArray = [];

// bad
anArray[anArray.length] = 'The line must be drawn here!';

// good
anArray.push('this far and no further!');
```

- 4.3 Use array spreads `...` to copy arrays.

```js
// bad
const len = items.length;
const itemsCopy = [];
let i;

for (i = 0; i < len; i += 1) {
    itemsCopy[i] = items[i];
}

// good
const itemsCopy = [...items];
```

- 4.4 To convert an iterable object to array, use spready `...` instead of `Array.from`.

```js
const foo = document.querySelectorAll('.foo');

// meh
const nodes = Array.from(foo);

// best
const nodes = [...foo];
```

- 4.5 Use `Array.from` for converting an array-like object to an array.

```js
const arrLike = {0: 'read', 1: 'the', 2: 'manual', length: 3 };

// bad
const arr = Array.prototype.slice.call(arrLike);

// good
const arr = Array.from(arrLike);
```

- 4.6 Use `Array.from` instead of spread `...` for mapping over iterables.

> This avoids creating an intermediate array.

```js
// bad
const baz = [...foo].map(bar);

// good
const baz = Array.from(foo, bar);
```

- 4.7 Use return statements in array method callbacks. It's ok to omit the return if the function body consists of a single statement returning an expression without side effects, following 8.2. eslint: `array-callback-return`

```js
// good
[1, 2, 3].map((x) => {
    const y = x + 1;
    return x - y;
});

// good
[1, 2, 3].map(x => x + 1);

// bad - no returned values makes 'acc' become undefined after the first iteration
[[0, 1], [2, 3], [4, 5]].reduce((acc, item, index) => {
    const flatten = acc.concat(item);
    acc[index] = flatten;
});

// good
[[0, 1], [2, 3], [4, 5]].reduce((acc, item, index) => {
  const flatten = acc.concat(item);
  acc[index] = flatten;
  return flatten;
});

// bad
inbox.filter((msg) => {
  const { subject, author } = msg;
  if (subject === 'Mockingbird') {
    return author === 'Harper Lee';
  } else {
    return false;
  }
});

// good
inbox.filter((msg) => {
    const { subject, author } = msg;
    if(subject === 'Mockingbird') {
        return author === 'Harper Lee';
    }

    return false;
});
```

- 4.8 Use line breaks after open and before close array brackets if an array has multiple lines.

```js
// bad
const arr = [
    [0, 1], [2, 3], [4, 5]
];

const objectInArray = [{
    id: 1,
}, {
    id: 2,
}];

const numberInArray = [
    1, 2,
];

// good
const arr = [[0, 1], [2, 3], [4, 5]];

const objectInArray = [
    {
        id: 1,
    },
    {
        id: 2,
    },
];

const numberInArray = [
    1,
    2,
];
```

## Destructuring

- 5.1 Use object destructuring when accessing and using multiple properties of an object. eslint `prefer-destructuring`

> Destructuring saves you from creating temporary references for those properties.

```js
// bad
function getFullName(user) {
  const firstName = user.firstName;
  const lastName = user.lastName;

  return `${firstName} ${lastName}`;
}

// good
function getFullName(user) {
  const { firstName, lastName } = user;
  return `${firstName} ${lastName}`;
}

// best
function getFullName({ firstName, lastName }) {
  return `${firstName} ${lastName}`;
}
```

- 5.2 Use array destructuring. elint: `prefer-destructuring`

```js
const arr = [1, 2, 3, 4];

// bad
const first = arr[0];
const second = arr[1];

// good
const [first, second] = arr;
```

- 5.3 Use object destructuring for multiple return values, not array destructuring.

> This is as you can add new properties over time or change the order of things without breaking call sites.

```js
// bad
function processInput(input) {
    // then a miracle happens
    return [left, right, top, bottom];
}

// the caller needs to think about the order of return data
const [left, __, top] = processInput(input);

// good
function processInput(input){
    // TODO: Perform a miracle
    return { left, right, top, bottom };
}

// the caller selects only the data needed now
const { left, top } = processInput(data);
```

## Strings

- 6.1 Use single quotes for strings. eslint: `quotes`

```js
// bad
const name = "Capt. Sisko";

// bad - template literals sould contain interpolation or new lines
const name = `Capt. Sisko`;

// good
const name = 'Capt.  Sisko';
```

- 6.2 String that cause a line to go over 100 characters should not be written across multiple lines using string concatenation

> Broken strings make code less searchable

```js
// bad
const errorMessage = 'This is a super long error that was thrown because \
of Batman. When you stop to think about how Batman had anything to do \
with this, you would get nowhere \
fast.';

// bad
const errorMessage = 'This is a super long error that was thrown because ' +
  'of Batman. When you stop to think about how Batman had anything to do ' +
  'with this, you would get nowhere fast.';

// good
const errorMessage = 'This is a super long error that was thrown because of Batman. When you stop to think about how Batman had anything to do with this, you would get nowhere fast.';
```

- 6.3 When programmatically building up strings, use template strings instead of concatenation. eslint: `prefer-template` `template-curly-spacing`

> Template strings are more readble and concise, it also gives you proper newlines and interpolation features

```js
// bad
function sayHi(name) {
    return 'How are you, ' + name + '?';
}

// bad
function sayHi(name) {
    return ['How are you, ', name, '?'].join();
}

// bad
function sayHi(name) {
    return `How are you, ${ name }?`;
}

// good
function sayHi(name) {
    return `How are you, ${name}?`;
}
```

- 6.4 Never use `eval()` on a string, it opens too many vulnerabilities. eslint: `no-eval`

- 6.5 Do not escape characters in strings unless absolutely necessary. eslint: `no-useless-escape`

> Backslashes harm redability and searchability and thus should only be present when needed.

```js
// bad
const foo = '\'this\' \i\s \"quoted\"';

// good
const foo = '\'this\' is "quoted"';
const foo = `my name is '${name}'`;
```

## Functions

- 7.1 Use named function expressions instead of function declarations. eslint: `func-style`

> Function declarations are hoisted, which makes it far too easy to reference a function before it is defined in a file. This harms readability as well as maintainability. If a function's definition is big enough or complex enough to be intefering with the understanding of the rest of a file it's probably time to extract it into its own module. Don’t forget to explicitly name the expression, regardless of whether or not the name is inferred from the containing variable (which is often the case in modern browsers or when using compilers such as Babel). This eliminates any assumptions made about the Error's call stack. The function name should be distinct from the variable name to allow for easier debugging and searching.

```js
// bad
function foo(){

}

// bad
const foo = function(){

}

// good
// lexical name distinguished from the variable-referenced invocation(s)
const short = function longUniqueMoreDescriptiveFoo(){

}
```

- 7.2 Wrap immediately invoked function expressions in parantheses. eslint: `wrap-iife`

> This is because an immediately invoked function expression is a single unit - wrapping both it and its invocation parens in parens, cleanly expresses this. However in a world with modules you should hardly ever need an IIFE.

```js
// iife
(function() {
    console.log('Tea. Earl Grey. Hot`);
}());
```

- 7.3 Never declare a function in a non-function block (`if`, `while`, etc). Assign the function to a variable instead. Browsers will allow you to do the former but they all interpret it differently which is no bueno. eslint: `no-loop-func`

- 7.4 **Note:*- ECMA-262 defines a `block` as a list of statements. A function declaration is not a statement.

```js
// bad
if (currentUser) {
    function test() {
        console.log('No.');
    }
}

// good
let test;
if (currentUser) {
    test = () => {
        console.log('Better!');
    };
}
```

- 7.5 Never name a parameter `arguments`. This will take precedence over the `arguments` object in given to every function scope.

```js
// bad
function foo(name, options, arguments) {

}

// good
function foo(name, options, args) {

}
```

- 7.6 Never use `arguments`, opt instead to use the rest syntax `...`. eslint: `prefer-rest-params`

> `...` is explicit about which arguments you want pulled. Also rest arguments are a real Array and not Array-like like `arguments`

```js
 // bad
 function concatenateAll() {
     const args = Array.prototype.slice.call(arguments);
     return args.join('');
 }

 // good
 function concatenateAll(...args){
     return args.join('');
 }
 ```

- 7.7 Use default parametere syntax rather than mutating function arguments.

```js
// really bad
function handleThings(opts) {
    // Please no! Don't mutate function arguments.
    // Double bad: if opts is falsy it'll be set to an object which can introduce subtle bugs
    opts = opts || {};
}

// Still rather bad to be honest
function handleThings(opts) {
    if (opts === void 0) {
        opts = {};
    }
}

// The dragon is sated
function handleThings(opts = {}) {

}
```

- 7.8 Avoid side effects with default params

> Reason? They are confusing to reason about!

```js
var b = 1;
// bad
function count(a = b++) {
    console.log(a);
}

count();    // 1
count();    // 2
count(3);   // 3
count();    // 3
```

- 7.9 Always put default parameters last.

```js
// bad
function handleIt(opts = {}, name){

}

// good
function handleIt(name, opts = {}) {

}
```

- 7.10 Never use the Function constructor to create a new function. eslint: `no-new-func`

> Creating a function like this evaluates a string similarly to `eval()` which opens up vulnerabilities

```js
// bad
var add = new Function('a', 'b', 'return a + b');

// Still bad
var subtract = new Function('a', 'b', 'return a - b');
```

- 7.11 Spacing in a function signature. eslint: `space-before-function-paren` `space-before-blocks`

> Because consistency is nice! Also you shouldn't have to add or remove a space when adding or removing a name.

```js
// bad
const f = function(){};
const g = function (){};
const h = function() {};

// good
const x = function () {};
const y = function a() {};
```

- 7.12 Never mutate parameters! eslint: `no-param-reassign`

> Manipulating objects passed in as parameters can cause unwanted variable side-effects in the original caller.

```js
// bad
function f1(obj) {
    obj.key = 1;
}

// good
function f2(obj) {
    const key = Object.prototype.hasOwnProperty.call(obj, 'key') ? obj.key : 1;
}
```

- 7.13 Never reassign parameters. eslint: `no-param-reassign`

> Reassigning parameters can lead to unexpected behaviour, especially when accessing the `arguments` object. It can also cause optimization issues, especially in V8, which is used in Chrome primarily among other softwares.

```js
// bad
function f1(a) {
    a = 1;
}

function f2(a) {
    if(!a) { a = 1; }
}

// good
function f3(a) {
    const b = a || 1;
}

function f4(a = 1) {
    // ...
}
```

* 7.14 Prefer the use of the spread operator `...` to call variadic functions. eslint: `prefer-spread`

> It's cleaner, you don't need to supply a context, and you can not easily compose `new` with `apply`.

```js
// bad
const x = [1, 2, 3, 4, 5];
console.log.apply(console, x);

// good
const x = [1, 2, 3, 4, 5];
console.log(...x);

// bad again
new (Function.prototype.bind.apply(Date, [null, 2016, 8, 5]));

// Bueno
new Date(...[2016, 8, 5]);
```

* 7.15 Functions with multiline signatures, or invocations, should be indented just like every other multiline list in this guide: with each item on a line by itself, with a trailing comma on the last item. eslint: `function-paren-newline`

```js
// bad
function foo(bar,
             baz,
             quux) {
    //...
}

// good
function foo(
    bar,
    baz,
    quux,
) {
    // ...
}

// bad again
console.log(foo,
    bar,
    baz);

// good again!
console.log(
    foo,
    bar,
    baz,
);
```

## Arrow Functions

- 8.1 When you must use an anonymous function (e.g. passing an inline callback), use arrow function notation. eslint: `prefer-arrow-callback`, `arrow-spacing`

> For: It creates a version of the function that executes in the context of `this`, which is usually what you want and with a more concise syntax!

> Against: If you have a fairly complicated function it might be better to move that logic in to its own named function expression instead.

```js
// bad
[1, 2, 3].map(function (x) {
    const y = x + 1;
    return x * y;
});

// good
[1, 2, 3].map((x) => {
    const y = x + 1;
    return x * y;
})
```

- 8.2 If a function body consists of a single statement returning an expression without side effects, omit the braces and use the implicit return. Otherwise, keep the braces and use a `return` statement. eslint: `arrow-parens`, `arrow-body-style`

> Why? Good ol' syntactic sugar. It reads well if mutliple functions are chained together.

```js
// bad
[1, 2, 3].map(number => {
    const nextNumber = number + 1;
    `A string containing the ${nextNumber}.`;
});

// good
[1, 2, 3].map(number => `A string containing the ${number}.`);

// good
[1, 2, 3].map((number) => {
    const nextNumber = number + 1;
    return `A string containing the ${nextnumber}.`;
});

// Also good!
[1, 2, 3].map((number, index) => ({
    [index]: number,
}))

// No implicit return with side effects
function foo(callback) {
    const val = callback();
    if(val === true) {
        // Do a thing here!
    }
}

let bool = false;

// bad
foo(() => bool = true);

// good
foo(() => {
    bool = true;
});
```

- 8.3 In case the expression spans over multiple lines, wrap it in parens for better readability

> This helps clearly show where the function starts and ends

```js
// bad
['get', 'post', 'put'].map(httpMethod => Object.prototype.hasOwnProperty.call(
        httpMagicObjectWithAVeryLongName,
        httpMethod,
    )
);

// Here's one I made earlier
['get', 'post', 'put'].map(httpMethod => (
    Object.prototype.hasOwnProperty.call(
        httpMagicObjectWithAVeryLongName,
        httpMethod,
    )
));_
```

- 8.4 If the function takes a single argument and doesn't use braces, omit the parentheses. Otherwise always include the parens around arguments for clarity and consistency. eslint: `arrow-parens`

> Less visual clutter

> We could also enforce that parentheses are always used if that's easier.

```js
// bad
[1, 2, 3].map((x) => x * x);

// good
[1, 2, 3].map(x => x * x);

// good
[1, 2, 3].map(number => (
    `Perhaps we have a long string where the ${number} is included. But perhaps that string is so long you don't want it on the .map line!`
))

// bad
[1, 2, 3].map(x => {
    const y = x + 1;
    return x * y;
});

// good
[1, 2, 3].map((x) => {
    const y = x + 1;
    return x * y;
})
```

- 8.5 Avoid confusing arrow function syntax ( `=>` ) with comparison operators(`<=`, `>=`). eslint: `no-confusing-arrow`

```js
// bad
const itemHeight = item => item.height > 256 ? item.largeSize : item.smallSize;

// still bad
const itemHeight = (item) => item.height > 256 ? item.largeSize : item.smallSize;

// good
const itemHeight = item => (item.height > 256 ? item.largeSize : item.smallSize);

// good
const itemHeight = (item) => {
    const { height, largeSize, smallSize } = item;
    return height > 256 ? largeSize : smallSize;
}
```

- 8.6 Enforce the location of arrow function bodies with implicit returns. eslint `implicit-arrow-linebreak`

```js
// bad
(foo) => 
    bar;

(foo) =>
    (bar);

// good
(foo) => bar;
(foo) => (bar);
(foo) => (
    bar
);
```

## Classes and Constructors

- 9.1 Always use `class`. Avoid manipulating `prototype` directly.

> `class` syntax is more concise and easier to reason about.

```js
// bad
function Queue(contents = []) {
    this.queue = [...contents];
}
Queue.prototype.pop = function () {
    const value = this.queue[0];
    this.queue.splice(0, 1);
    return value;
};

// good
class Queue {
    constructor(contents = []) {
        this.queue = [...contents];
    }
    pop() {
        const value = this.queue[0];
        this.queue.splice(0, 1);
        return value;
    }
}
```

- 9.2 Use `extends` for inheritance.

> It is a built-in way to inherit prototype functionality without breaking `instanceof`.

```js
// bad
const inherits = require('inherits');
function PeekableQueue(contents) {
    Queue.apply(this, contents);
}
inherits(PeekableQueue, Queue);
PeekableQueue.prototype.peek = function () {
    return this.queue[0];
};

// good
class PeekableQueue extends Queue {
    peek() {
        return this.queue[0];
    }
}
```

- 9.3 Methods can return `this` to help with method chaining.

```js
// bad
Jedi.prototype.jump = function () {
    this.jumping = true;
    return true;
};

Jedi.prototype.setHeight = function (height) {
    this.height = height;
};

const luke = new Jedi();
luke.jump(); // => true
luke.setHeight(20); // => undefined

// good
class Jedi {
    jump() {
        this.jumping = true;
        return this;
    }

    setHeight(height) {
        this.height = height;
        return this;
    }
}

const luke = new Jedi();

luke.jump()
    .setHeight(20);
```

- 9.4 It's okay to write a custom `toString()` method, just make sure it works successfully and causes no side effects.

```js
class Jedi {
    constructor(options = {}) {
        this.name = options.name || 'no name';
    }

    getName() {
        return this.name;
    }

    toString() {
        return `Jedi - ${this.getName()}`;
    }
}
```

- 9.5 Classes have a default constructor if one is not specified. An empty constructor function or one that just delegates to a parent class is unnecessary. eslint: `no-useless-constructor`

```js
// bad
class Jedi {
    constructor() {}

    getName() {
        return this.name;
    }
}

// bad
class Obi extends Jedi {
    constructor(...args) {
        super(...args);
    }
}

// good
class Obi extends Jedi {
    constructor(...args) {
        super(...args);
        this.name = 'Obi'
    }
}
```

- 9.6 Avoid duplicate class members. eslint: `no-dupe-class-members`

> Duplicate class member declarations will silently prefer the last one - having duplicates is almost certainly a bug.

```js
// bad
class Foo {
    bar() { return 1; }
    bar() { return 2; }
}

// good
class Foo {
    bar() { return 1; }
}

// good
class Foo {
    bar() { return 2; }
}
```

## Modules

- 10.1 Always use modules (`import`/`export`) over a non-standard module system. You can always transpile to your preferred module system if needed.

> Modules are the future! Let's start using the future now.

```js
// bad
const axios = require('axios');
module.exports = axios.post;

// ok
import axios from 'axios'
export default axios.post;

// best
import { post } from axios;
export default post;
```

- 10.2 Do not use wildcard imports.

> This makes sure there is only a single default export.

```js
// bad
import * as axios from 'axios';

// good
import axios from 'axios';
```

- 10.3 And do not export directly from an import.

> The one-liner is concise granted, however having one clear way to import and one clear way to export ensures consistency.

```js
// bad
// filename post.js
export { post as default } from 'axios';

// good
// filename post.js
import { post } from 'axios';
export default post;
```

- 10.4 Only import from a path in one place. eslint: `no-duplicate-imports`

> Having multiple lines that import from the same path can make code harder to maintain in the long run.

```js
// bad
import axios from 'axios';
// more imports //
import { post, get } from 'axios';

// good
import axios, { post, get } from 'axios';

// good
import axios, {
    post,
    get,
} from 'axios';
```

- 10.5 Do not export mutable bindings. eslint: `import/no-mutable-exports`

> Mutation should be avoided generally as a rule of thumb. In particular however when eporting mutable bindings. While this technique may be needed for some edge cases, in general, only constant references should be exported;

```js
// bad
let foo = 3;
export { foo };

// good
const foo = 3;
export { foo };
```

- 10.6 In modules with a single export, prefer default export over a named export. eslint: `import/prefer-default-export`

> To encourage more files that only ever export on thing, this is better for readability and maintainability.

```js
// bad
export function foo() {}

// good
export default function foo() {}
```

- 10.7 Keep all `import` statements above non-import statements. eslint: `import/first`

> As 'import' statements are hoisted keeping them at the top of a file prevents unexpected behavior.

```js
// bad
import foo from 'foo';
foo.init();

import bar from 'bar';

// good
import foo from 'foo';
import bar from 'bar';

foo.init();
```

- 10.8 Multiline imports should be indented exactly like multiline arrays and object literals.

> In the interests of consistency curly braces all follow the same indentation and the trailing comma is used exactly as it is elsewhere.

```js
// bad
import {longNameA, longNameB, longNameC, longNameD, longNameE} from 'path';

// good
import {
    longNameA,
    longNameB,
    longNameC,
    longNameD,
    longNameE,
} from 'path';
```

- 10.9 Disallow Webpack loader syntax in module import statements. eslint: `import/no-webpack-loader-syntax`

> Using Webpack syntax couples the code to a module bundler. Prefer using the loader syntax in the webpack.config.js file instead.

```js
// bad
import fooSass from 'css!sass!foo.scss';
import barCss from 'style!css!bar.css';

// good
import fooSass from 'foo.scss';
import barCss from 'bar.css';
```

## Iterators and Generators

- 11.1 Don't use iterators. Prefer JavaScript's higher-order functions instead of loops like `for-in` or `for-of`. eslint: `no-iterator`, `no-restricted-syntax`

> This enforces the immutable rule. Dealing with pure functions that return values is easier to reason about than side effects.

> Use `map()` / `every()` / `filter()` / `find()` / `findIndex()` / `reduce()` / `some()` / ... to iterate over arrays, and `Object.keys()` / `Object.values()` / `Object.entries()` to produce arrays so you can iterate over objects.

```js
const numbers = [1, 2, 3, 4, 5];

// bad
let sum = 0;
for (let num of numbers) {
    sum += num;
}
sum === 15;

// good
let sum = 0;
numbers.forEach((num) => {
    sum += num;
});
sum === 15;

// best (use the functional force)
const sum = numbers.reduce((total, num) => total + num, 0);
sum === 15;

// bad
const increasedByOne = [];
for (let i = 0; i < numbers.length; i++) {
    increasedByOne.push(numbers[i] + 1);
}

// good
const increasedByOne = [];
numbers.forEach((num) => {
    increasedByOne.push(num + 1);
});

// best (keep it nice and functional)
const increasedByOne = numbers.map(num => num + 1);
```

- 11.2 Don't use generators for now.

> They don't transpile well to ES5.

- 11.3 If you absolutely positively have to use a generator make sure their function signature is spaced properly. eslint: `generator-star-spacing`

> `function` and `*` are part of the same conceptual keyword - `*` is not a modifier for `function`, `function*` is a unique construct different from `function`.

```js
// bad
function * foo() {

}

// bad
const bar = function * () {

};

// bad
const baz = function *() {

};

// bad
const quux = function*() {

};

// bad
function*foo() {

}

// also bad
function *foo() {

}

// really bad
function
*
foo() {

}

// also really bad
const wat = function
*
() {

};

// good
function* foo() {

}

// good
const foo = function* () {

};
```
## Properties

- 12.1 Use dot notation when accessing properties. eslint: `dot-notation`

```js
const sisko = {
    rank: 'captain',
    ship: 'defiant',
}

// bad
const siskoRank = sisko['rank'];

// good
const siskoRank = sisko.rank;
```

- 12.2 Use bracket notation `[]` when accessing properties with a variable.

```js
const sisko = {
    rank: 'captain',
    ship: 'defiant',
}

function getProp(prop) {
    return sisko[prop];
}

const siskoRank = getProp('rank');
```

- 12.3 Use exponentiation operator `**` when calculating exponentiations. eslint: `no-restricted-properties`

```js
// bad
const binary = Math.pow(2, 10);

// good
const binary = 2 ** 10;
```

## Variables

- 13.1 Always use `const` or `let` to declare variables. Not doing so results in global variables. We would like to avoid polluting the global namespace. Remember Captain Planet warned us of the dangers of global pollution. eslint: `no-undef`, `prefer-const`

```js
// bad
superPower = new SuperPower();

// good
const superPower = new SuperPower();
```

- 13.2 Use one `const` or `let` declaration per variable. eslint: `one-var`

> It's easier to add new variable declarations this way, and you never have to worry about swapping out a `;` for a `,` or introducing punctuation-only diffs. You can also step through each declaration with the debugger, instead of jumping through all of them at once.

```js
// bad
const items = getItems(),
    goSportsTeam = true,
    deepSpace = 9;

// bad
// (Compare with above and see if you can spot the mistake)
const items = getItems(),
    goSportsTeam = true;
    deepSpace = 9;

// good
const items = getItems();
const goSportsTeam = true;
const deepSpace = 9;
```

- 13.3 Group all `const` and then group all `let` variables.

> This is helpful when later on you might want to assign a variable depending on one of the previous assigned variables.

```js
// bad
let i, len, deepSpace,
    items = getItems(),
    goSportsTeam = true;
    
// bad
let i;
const items = getItems();
let len;
const goSportsTeam = true;
let deepSpace;

// good
const items = getItems();
const goSportsTeam = true;
let i;
let len;
let deepSpace;
```

- 13.4 Assign variables where you need them, but place them in a reasonable place.

> `let` and `const` are block scoped and not function scoped.

```js
// bad - unneccessary function call
function checkName(hasName) {
    const name = getName();

    if (hasName === 'test') {
        return false;
    }

    return name;
}

// good
function checkName(hasName) {
    if(hasName === 'test') {
        return false;
    }

    const name = getName();

    if (name === 'test') {
        this.setName('');
        return false;
    }

    return name;
}
```

- 13.5 Don't chain variable assignments. eslint: `no-multi-assign`

> Chaining variable assignments creates implicit global variables.

```js
// bad
(function example() {
    // Javascript interprets this as
    // let a = ( b = ( c = 1) );
    // the let keyword only applies to variable a; variables b and c become global.
    let a = b = c = 1;
}());

console.log(a); // throws ReferenceError
console.log(b); // 1
console.log(c); // 1

// good
(function example() {
    let a = 1;
    let b = a;
    let c = a;
}());

console.log(a); // throws ReferenceError
console.log(b); // throws ReferenceError
console.log(c); // throws ReferenceError

// the same applies for const
```

- 13.6 Avoid using unary increments and decrements (`++`, `--`). eslint: `no-plusplus`

> Unary increment and decrement statements are subject to automatic semicolon insertion and can cause silent errors with incrementing or decrementing a value within an application. It is also more expressive to mutate values like so: `num += 1` rather than `num++`. This also prevents pre-incrementing and pre-decrementing which can cause unexpected behavior

```js
// bad
const array = [1, 2, 3];
let num = 1;
num++;
--num;

let sum = 0;
let truthyCount = 0;
for(let i = 0; i < array.length; i++) {
    let value = array[i];
    sum += value;
    if (value) {
        truthyCount++;
    }
}

// good
const array = [1, 2, 3];
let num = 1;
num += 1;
num -= 1;

const sum = array.reduce((a, b) => a + b, 0);
const truthyCount = array.filter(Boolean).length;
```

- 13.7 Avoid linebreaks before or after `=` in an assignment. If your assignment violates `max-len` surround the value in parens. eslint `operator-linebreak`.

> Linebreaks surrounding `=` can obfuscate the value of an assignment.

```js
// bad
const foo =
    superLongLongLongLongLongLongLongLongFunctionName();

// bad
const foo
    = 'superLongLongLongLongLongLongLongString';

// good
const foo = (
    superLongLongLongLongLongLongLongLongLongFunctionName()
);

// good
const foo = 'superLongLongLongLongLongLongLongString';
```

- 13.8 Disallow unused variables. eslint: `no-unused-vars`

> Variables that are declared and not used anywhere in the code are most likely an error due to incomplete refactoring. Such variables take up space in the code and can lead to confusion.

```js
// bad

var some_unused_var = 42;

// Write-only variables are not considered as used.
var y = 10;
y = 5;

// A read for a modification of itself is not considered as used.
var z = 0;
z = z + 1;

// Unused function arguments.
function getX(x, y) {
    return x;
}

// good

function getXPlusY(x, y) {
  return x + y;
}

var x = 1;
var y = a + 2;

alert(getXPlusY(x, y));

// 'type' is ignored even if unused because it has a rest property sibling.
// This is a form of extracting an object that omits the specified keys.
var { type, ...coords } = data;
// 'coords' is now the 'data' object without its 'type' property.
```

## Hoisting

- 14.1 `var` declarations get hoisted to the top of their closest enclosing function scope, their assignment does not. `const` and `let` declarations have something called Temporal Dead Zones. This means that `typeof` is no longer 100% safe.

```js
// we know this wouldn’t work (assuming there
// is no notDefined global variable)
function example() {
  console.log(notDefined); // => throws a ReferenceError
}

// creating a variable declaration after you
// reference the variable will work due to
// variable hoisting. Note: the assignment
// value of `true` is not hoisted.
function example() {
  console.log(declaredButNotAssigned); // => undefined
  var declaredButNotAssigned = true;
}

// the interpreter is hoisting the variable
// declaration to the top of the scope,
// which means our example could be rewritten as:
function example() {
  let declaredButNotAssigned;
  console.log(declaredButNotAssigned); // => undefined
  declaredButNotAssigned = true;
}

// using const and let
function example() {
  console.log(declaredButNotAssigned); // => throws a ReferenceError
  console.log(typeof declaredButNotAssigned); // => throws a ReferenceError
  const declaredButNotAssigned = true;
}
```

- 14.2 Anonymous function expressions hoist their variable name, but not the function assignment.

```js
function example() {
    console.log(anonymous); // => undefined

    anonymous(); // => TypeError anonymous is not a a functions

    var anonymous = function () {
        console.log('anonymous functions go!')
    };
}
```

- 14.3 Named function expressions hoist the variable name, not the function name of the function body.

```js
function example() {
    console.log(named); // undefined

    named(); // TypeError named is not a function

    superPwower(); // ReferenceErrpr superPower is note

    var named = function superPower() {
        console.log('Flying')
    }
}

// the same is true when the function name
// is the same as the variable name.
function example() {
  console.log(named); // => undefined

  named(); // => TypeError named is not a function

  var named = function named() {
    console.log('named');
  };
}
```

- 14.4 Function declarations hosit their name and the function body.

```js
function example() {
    superPower(); // => Flying

    fuction superPower() {
        console.log('Flying');
    }
}
```

## Comparison Operators & Equality

- 15.1 Use `===` and `!==` over `==` and `!=`. eslint: `eqeqeq`

- 15.2  Conditional statements such as the `if` statement evaluate their expression using coercion with the `ToBoolean` abstract method and always follow these simple rules: 

    - **Objects** evaluate to **true**
    - **Undefined** evauluates to **false**
    - **Null** evaluates to **false**
    - **Booleans** evaluate to **the value of the boolean**
    - **Numbers** evaluate to **false** if **+0, -0, or NaN**, otherwise **true**
    - **Strings** evaluate to **false** if an empty string `''`, otherwise **true**

```js
if([0] && []) {
    // true
    // an array (even empty) is an object, objects will evaluate to true
}
```

- 15.3 Use shortcuts for booleans, but explicit comparisons for strings and numbers.

```js
// bad
if (isValid === true) {}

// good
if (isValid) {}

// bad
if (name) {}

// good
if (name !== '') {}

// bad
if (collection.length) {}

// good
if (collection.length > 0) {}
```

- 15.4 Use braces to create blocks in `case` and `default` clauses that contain lexical declaration (e.g. `let`, `const`, `function` and `class`). eslint: `no-case-declarations`

> Lexical declarations are visible in the entire `switch` block but only get initialized when assigned, which only happens when its `case` is reached. This causes problems when multiple `case` clauses attempt to define the same thing.

```js
// bad
switch (foo) {
    case 1:
        let x = 1;
        break;
    case 2:
        const y = 2;
        break;
    case 3:
        function f() {

        }
        break;
    default:
        class C {}
}

// good
switch (foo) {
    case 1: {
        let x = 1;
        breal;
    }
    case 2: {
        const y = 2;
        break;
    }
    case 3: {
        function f() {

        }
        break;
    }
    case 4: {
        bar();
        break;
    }
    default: {
        class C {}
    }
}
```

- 15.6 Ternaries should not be nested and generally should be single line expressions. eslint: `no-nested-ternary`

```js
// bad
const foo = maybe1 > maybe 2
    ? "bar"
    : value1 > value2 ? "baz" : null;

// split into 2 seperated ternary expressions
const maybeNull = value1 > value 2 ? 'baz' : null;

// better
const foo = maybe1 > maybe2
    ? 'bar'
    : maybeNull;

// best
const foo = maybe1 > maybe2 ? 'bar' : maybeNull;
```

- 15.7 Avoid unneeded ternary operators. eslint: `no-unneeded-ternary`

```js
// bad
const foo = a ? a : b;
const bar = c ? true : false;
const baz = c ? false : true;

// good
const foo = a || b;
const bar = !!c;
const baz = !c;
```

- 15.8 When mixing operators, enclose them in parentheses. The only exception is the stardard arithmetic operators (`+`, `-`, `*` and `/`) since their precedence is broadly understood. eslint: `no-mixed-operators`.

> This improves readability and clarifies a developer's intent.

```js
// bad 
const foo = a && b < 0 || c > 0 || d + 1 === 0;

// bad
const bar = a ** b - 5 % d;

// bad
// Could be seen as (a || b) && c
if (a || b && c) {
    return d;
}

// good
const foo = (a && b < 0) || c > 0 || (d + 1 === 0);

// good
const bar = (a ** b) - (5 % d);

// good
if (a || (b && c)) {
    return d;
}

// good
const bar = a + b / c * d;
```

## Blocks

- 16.1 Use braces with all multi-line block. eslint: `nonblock-statement-body-position`

```js
// bad
if (test)
    return false;

// good
if (test) return false;

// good
if (test) {
    return false;
}

// bad
function foo() { return false; }

// good
function bar() {
    return false;
}
```

- 16.2 If you're using multi-line blocks with `if` and `else`, put else  on the same line as your `if` block's closing brace. eslint: `brace-style`

```js
// bad
if (test) {
    thing1();
    thing2();
}
else {
    thing3();
}

// good
if (test) {
    thing1();
    thing2();
} else {
    thing3();
}
```

- 16.3 If an `if` block always executes a `return` statement, the subsequent `else` block is unnecessary. A `return` in an `else if` block following an `if` block that contains a `return` can be seperated into multiple `if` blocks. eslint: `no-else-return`

```js
// bad
function foo() {
    if (x) {
        return x;
    } else {
        return y;
    }
}

// bad
function cats() {
    if (x) {
        return x;
    } else if (y) {
        return y;
    }
}

// bad
function dogs() {
    if (x) {
        return x;
    } else {
        if (y) {
            return y;
        }
    }
}

// good
function foo() {
    if (x) {
        return x;
    }

    return y;
}

// good
function cats() {
    if (x) {
        return x;
    }

    if (y) {
        return y;
    }
}

// good
function dogs(x) {
    if (x) {
        if (z) {
            return y;
        }
    } else {
        return z;
    }
}
```

## Control Statements

- 17.1 In case your control statement (`if`, `while`, etc.) gets too long or exceeds the maximum line length, each (grouped) condition could be put into a new line. The logical operator should begin the line.

> Requiring operators at the beginning of the line keeps the operators aligned and follows a pattern similar to method chaining. This also improves readability by making it easier to visually follow complex logic.

```js
// bad
if((foo === 123 || bar === 'abc') && doesItLookGoodThisLong() && ohGodWhyPleaseStop()) {
    thing1();
}

// bad
if(foo === 123 &&
    bar === 'abc') {
        thing1();
}

// bad
if (foo === 123
    && bar === 'abc'){
        thing1();
}

// bad
if (
    foo === 123 &&&
    bar === 'abc'
) {
    thing1();
}

// good
if (
    foo === 123
    && bar === 'abc'
) {
    thing1();
}

// good
if (
    (foo === 123 || bar === 'abc')
    && doesItLookGoodThisLong()
    && ohGodWhyPleaseStop()
) {
    thing1();
}

// good
if (foo === 123 && bar === 'abc') {
    thing1();
}
```

- 17.2 Don't use selection operators in place of control statements.

```js
// bad
!isRunning && startRunning();

// good
if (!isRunning) {
    startRunning();
}
```

## Comments

- 18.1 Use `/** .... */` for multi-line comments.

```js
// bad
// make() returns a new element
// based on the passed in tag name
//
// @param {String} tag
// @return {Element} element
function make(tag) {

  // ...

  return element;
}

// good
/**
 * make() returns a new element
 * based on the passed-in tag name
 */
function make(tag) {

  // ...

  return element;
}
```

- 18.2 Use `//` for single line comments. Place the comment on a newline above the subject of the comment. Put an empty line before the comment unless it's on the first line of a block

```js
// bad
const active = true; // is current tab

// good
// is current tab
const active = true;

// bad
function getType() {
    console.log('fetching type...');
    // set the default type to 'no type'
    const type = this.type || 'no type';
    return type;
}

// good
function getType() {
    console.log('fetching type...');

    // set the default type to 'no type'
    const type = this.type || 'no type';

    return type;
}

// also good
function getType() {
    //set the default type to 'no type'
    const type = this.type || 'no type';

    return type;
}
```

- 18.3 Start all comments with a space to make it easier to read. eslint: `spaced-comment`

```js
// bad
//is current tab
const active = true;

// good
// is current tab
const active = true;

// bad
/**
 *make() returns a new element
 *based on the passed-in tag name
 */
function make(tag) {

  // ...

  return element;
}

// good
/**
 * make() returns a new element
 * based on the passed-in tag name
 */
function make(tag) {

  // ...

  return element;
}
```

- 18.4 Prefixing your comments with `FIXME` or `TODO` helps other developers quickly ascertain what is being pointed out / needs to be done. These are different to other comments as they are actionable.

- 18.5 Use `// FIXME:` to annotate problems.

```js
class Calculator extends Abacus {
    constructor() {
        super();

        // FIXME: shouldn't use a global here
        total = 0;
    }
}
```

- 18.6 Use `// TODO:` to annotate solutions to problems.

```js
class Calculator extends Abacus {
    constructor() {
        super();

        // TODO: total should be configurable by an options param
        this.total = 0;
    }
}
```

## Whitespace

- 19.1 Use soft tabs (space character) set to 2 spaces. eslint: `indent`

```js
// bad
function foo() {
....let name;
}

// also bad
function foo() {
.let name;
}

// good
function bar() {
..let name;
}
```

- 19.2 Place 1 space before the leading brace. eslint: `space-before-blocks`

```js
// bad
function test(){
  console.log('test');
}

// good
function test() {
  console.log('test');
}

// bad
dog.set('attr',{
  age: '1 year',
  breed: 'Bernsese Mountain Dog'
});

// good
dog.set('attr', {
  age: '1 year',
  breed: 'Bernese Mountain Dog'
})
```

- 19.3 Place 1 space before the opening parenthesis in control statements (`if`, `while`, etc.). Place no space between the argument list and trhe function name in function calls and declarations. eslint: `keyword-spacing`.

```js
// bad
if(isJedi) {
  fight();
}

// good
if (isJedi) {
  fight();
}

// bad
function fight () {
  console.log('Swoosh!');
}

// good
function fight() {
  console.log('Swoosh!');
}
```

- 19.4 Set off operators with spaces. eslint: `space-infix-ops`

```js
// bad
const x=y+5;

// good
const x = y + 5;
```

- 19.5 End Files with a single newline character. eslint: `eol-last`

```js
// bad
import { post } from 'axios';
// ....
export default post;
```

```js
// bad
import { post } from 'axios';
// ....
export default post;↵
↵

```

```js
// good
import { post } from 'axios';
// ....
export default post;↵

```

- 19.6 Use indentation when making long method chains (more than 2 method chains). Use a leading dot, which emphasizes that the line is a method call, not a new statement. eslint: `newline-per-chained-call` `no-whitespace-before-property`

```js
// bad
$('#items').find('.selected').highlight().end().find('.open').updateCount();

// bad
$('#items').
  find('.selected').
    highlight().
    end().
  find('.open').
    updateCount();

// good
$('#items')
  .find('.selected')
    .highlight()
    .end()
  .find('.open')
    .updateCount();

// bad
const leds = stage.selectAll('.led').data(data).enter().append('svg:svg').classed('led', true)
    .attr('width', (radius + margin) * 2).append('svg:g')
    .attr('transform', `translate(${radius + margin},${radius + margin})`)
    .call(tron.led);

// good
const leds = stage.selectAll('.led')
    .data(data)
  .enter().append('svg:svg')
    .classed('led', true)
    .attr('width', (radius + margin) * 2)
  .append('svg:g')
    .attr('transform', `translate(${radius + margin},${radius + margin})`)
    .call(tron.led);

// good
const leds = stage.selectAll('.led').data(data);

```

- 19.7 Leave a blank line after blocks and before the next statement.

```js
// bad
if (foo) {
  return bar;
}
return baz;

// good
if (foo) {
  return bar;
}

return baz;

// bad
const obj = {
  foo() {

  },
  bar() {

  },
};
return obj;

// good
const obj = {
  foo() {

  },

  bar() {

  },
};

return obj;

// bar
const arr = [
  function foo() {

  },
  function bar() {

  },
];
return arr;

// good
const arr = [
  function foo() {
  },

  function bar() {
  },
];

return arr;

```

- 19.8 Do not pad your blocks with blank lines. eslint: `padded-blocks`

```js
// bad
function bar() {

  console.log(foo);

}

// bad
if (baz) {

  console.log(qux);
} else {
  console.log(foo);

}

// good
function bar() {
  console.log(foo);
}

// good
if (baz) {
  console.log(qux);
} else {
  console.log(foo);
}

```

- 19.9 Do not add spaces inside parentheses. eslint: `space-in-parens`

```js
// bad
function bar( foo ) {
  return foo;
}

// good
function bar(foo) {
  return foo;
}

// bad
if ( foo ) {
  console.log(foo);
}

// good
if (foo) {
  console.log(foo);
}

```

- 19.10 Do not add spaces indsde brackets. eslint: `array-bracket-spacing`

``` js
// bad
const foo = [ 1, 2, 3 ];
console.log(foo[ 0 ]);

// good
const foo = [1, 2, 3];
console.log(foo[0]);
```

- 19.11 Add spaced inside curly braces. eslint: `object-curly-spacing`

```js
// bad
const foo = {benjamin: 'sisko'};

// good
const foo = { benjamin: 'sisko' };
```

- 19.12 Avoid having lines of code that are longer than 100 characters (including whitespace). Note: per above, long strings are exempt from this rule and should not be broken up. eslint: `max-len`

> This ensures readability and maintainability.

```js
// bad
const foo = jsonData && jsonData.foo && jsonData.foo.bar && jsonData.foo.bar.baz && jsonData.foo.bar.baz.quux && jsonData.foo.bar.baz.quux.xyzzy;

// bad
$.ajax({ method: 'POST', url: 'https://socket.mcoombes.online/', data: { name: 'JC' } }).done(() => console.log('Congratulations!')).fail(() => console.log('What a shame.'));

// good
const foo = jsonData
  && jsonData.foo
  && jsonData.foo.bar
  && jsonData.foo.bar.baz
  && jsonData.foo.bar.baz.quux
  && jsonData.foo.bar.baz.quux.xyzzy;

// good
$.ajax({
  method: 'POST',
  url: 'https://socket.mcoombes.online/',
  data: { name: 'JC' },
})
  .done(() => console.log('Congratulations!'))
  .fail(() => console.log('What a shame.'));
```

- 19.13 Require consistent spacing inside an open block token and the next token on the same line. This rule also enforces consistent spacing inside a close block token and a previous token on the same line. eslint: `block-spacing`

```js
// bad
function foo() {return true;}
if (foo) { bar=0;}

// good
function foo() { return true; }
if (foo) { bar = 0; }
```

- 19.14 Avoid spaces before commas and require a space after commas. eslint: `comma-spacing`

```js
// bad
var foo = 1,bar = 2;
var arr = [1 , 2];

// good
var foo = 1, bar = 2;
var arr = [1, 2];
```

- 19.15 Enforce spacing inside of computed properties. eslint: `computed-property-spacing`

```js
// bad
obj[foo ]
obj[ 'foo']
var x = {[ b ]: a}
obj[foo[ bar ]]

// good
obj[foo]
obj['foo']
var x = { [b]: a }
obj[foo[bar]]

```

- 19.16 Enforce spacing between functions and their invocations. eslint: `func-call-spacing`

```js
// bad
func ();

func
();

// good
func();

```

- 19.17 Enforce spacing between keys and values in object literal properties. eslint: `key-spacing`

```js
// bad
var obj = { "foo" : 42 };
var obj2 = { "foo":42 };

// good
var obj = { "foo": 42 };

```

- 19.18 Avoid trailing spaces at the end of lines. eslint: `no-trailing-spaces`

- 19.19 Avoid multiple empty lines and only allow one newline at the end of files. eslint: `no-multiple-empty-lines`

```js
// bad
var x = 1;



var y = 2;

// good
var x = 1;

var y = 2;

```

## Commas

- 20.1 Leading commas: **No-No**. eslint: `comma-style`

```js
// bad
const story = [
  once
  , upon
  , aTime
]

// good
const story = [
  once,
  upon,
  aTime,
]

// bad
const hero = {
  firstName: 'Benjamin'
  , lastName: 'Sisko',
  , rank: 'Captain',
  , ship: 'USS Defiant'
}

// good
const hero = {
  firstName: 'Benjamin',
  lastName: 'Sisko',
  rank: 'Captain',
  ship: 'USS Defiant',
}
```

- 20.2 Additional trailing comma: **Yes please!**. eslint: `comma-dangle`

> This leads to cleaner git diffs. Also transpilers such as Babel remove the trailing comma in the transpiled code so you don't have to worry about the problems this can cause in quirks mode in older IE versions such as 7,8 and 9.

```js
// bad - git diff without trailing comma
const hero = {
  firstName: 'Alan',
- lastName: 'Turing'
+ lastName: 'Turing',
+ inventorOf: ['Turing Machine', 'Turing Test']
};

// good - git diff with trailing comma
const hero = {
  firstName: 'Alan',
  lastName: 'Turing',
+ inventorOf: ['Turing Machine', 'Turing Test'],
};
```

```js
// bad
const hero = {
  firstName: 'Jean-Luc',
  lastName: 'Picard'
};

const heroes = [
  'Batman',
  'Superman'
];

// good
const hero = {
  firstName: 'Jean-Luc',
  lastName: 'Picard',
};

const heroes = [
  'Batman',
  'Superman',
];

// bad
function createHero(
  firstName,
  lastName,
  inventorOf
) {
  // does nothing
}

// good
function createHero(
  firstName,
  lastName,
  inventorOf,
) {
  // does nothing
}

// good (note that a comma must not appear after a "rest" element)
function createHero(
  firstName,
  lastName,
  inventorOf,
  ...heroArgs
) {
  // does nothing
}

// bad
createHero(
  firstName,
  lastName,
  inventorOf
);

// good
createHero(
  firstName,
  lastName,
  inventorOf,
);

// good (note that a comma must not appear after a "rest" element)
createHero(
  firstName,
  lastName,
  inventorOf,
  ...heroArgs
);

```

## Semicolons

- 21.1 **Yup.** eslint: `semi`

> When JS encouters a line break without a semicolon it uses a set of rules called automatic semicolon insertion to determine whether or not it should regard that line break as the end of a statement and if it believes it is it inserts the semicolon as the name suggests. ASI contains a few eccentric behaviors, though, and your code will break if JavaScript misinterprets your line break. These rules will become more complicated as new features become a part of JavaScript. Explicitly terminating your statements and configuring your linter to catch missing semicolons will help prevent you from encountering issues.

```js
// bad - raises exception
const luke = {}
const leia = {}
[luke, leia].forEach(jedi => jedi.father = 'vader')

// bad - raises exception
const reaction = "No! That's impossible!"
(async function meanwhileOnTheFalcon() {
  // handle `leia`, `lando`, `chewie`, `r2`, `c3p0`
  // ...
}())

// bad - returns `undefined` instead of the value on the next line - always happens when `return` is on a line by itself because of ASI!
function foo() {
  return
    'search your feelings, you know it to be foo'
}

// good
const luke = {};
const leia = {};
[luke, leia].forEach((jedi) => {
  jedi.father = 'vader';
});

// good
const reaction = "No! That's impossible!";
(async function meanwhileOnTheFalcon() {
  // handle `leia`, `lando`, `chewie`, `r2`, `c3p0`
  // ...
}());

// good
function foo() {
  return 'search your feelings, you know it to be foo';
}

```

## Type Casting & Coercion

- 22.1 Type coercion should be performed at the begining of a statement.

- 22.2 String: eslint: `no-new-wrappers`

```js
// => this.reviewScore = 9;

// bad
const totalScore = new String(this.reviewScore); // typeof totalScore is "object" not "string".

// bad
const totalScore = this.reviewScore + ''; // invokes new this.reviewScore.valueOf()

// bad
const totalScore = this.reviewScore.toString(); // isn't guaranteed to return a string

// good
const totalScore = String(this.reviewScore);

```

- 22.3 Numbers: Use `Number` for type casting and `parseInt` always with a radix for parsing strings. eslint: `radix` `no-new-wrappers`

```js
const inputValue = '4';

// bad
const val = new Number(inputValue);

// bad
const val = +inputValue;

// bad
const val = inputValue >> 0;

// bad
const val = parseInt(inputValue);

// good
const val = Number(inputValue);

// good
const val = parseInt(inputValue, 10);

```

- 22.4 If for any reason you are doing something wild and `parseInt` is your bottleneck and need to use Bitshift for performance reasons, leave a comment explaining why and what is happening.

```js
// good
/**
 * parseInt was the reason my code was slow.
 * Bitshifting the String to coerce it to a
 * Number made it a lot faster.
 */
const val = inputValue >> 0;
```

- 22.5 **Note**: Be careful when using bitshift operations. Numbers are represented as 64-bit values, but bitshift operations always return a 32-bit integer. Bitshift can lead to unexpected behavior for integer values larger than 2,147,483,647 (largest signed 32 bit integer)

```js
2147483647 >> 0; // => 2147483647
2147483648 >> 0; // => -2147483648
2147483649 >> 0; // => -2147483647
```

- 22.6 Booleans: eslint: `no-new-wrappers`

```js
const age = 0;

// bad
const hasAge = new Boolean(age);

// good
const hasAge = Boolean(age);

// best
const hasAge = !!age;

```

## Naming Conventions

- 23.1 Avoid single letter names. Be descriptive with naming. eslint: `id-length`

```js
// bad
function q() {}

// good
function query() {}
```

- 23.2 Use camelCase when naming objects, functions and instances. eslint: `camelcase`

```js
// bad
const OBJECTttssss = {};
const this_is_my_object = {}
function c() {}

// good
const thisIsMyObject = {};
function thisIsMyFunction() {}

```

- 23.3 Use PascalCase only when naming constructors or classes. eslint: `new-cap`

```js
// bad
function user(options) {
  this.name = options.name;
}

const bad = new user({
  name: 'nope',
});

// good
class User {
  constructor(options) {
    this.name = options.name;
  }
}

const good = new User({
  name: 'Yeeee',
});

```

- 23.4 Do not use trailing or leading underscores. eslint: `no-underscore-dangle`

> JS does not have the concept of privacy in terms of props or methods. Although a leading underscore is a common convention to mean “private”, in fact, these properties are fully public, and as such, are part of your public API contract. This convention might lead developers to wrongly think that a change won’t count as breaking, or that tests aren’t needed. tl;dr: if you want something to be “private”, it must not be observably present.

```js
// bad
this.__firstName__ = 'Panda';
this.firstName_ = 'Panda';
this._firstName = 'Panda';

// good
this.firstName = 'Panda';

// good, in environments where WeakMaps are available
// see https://kangax.github.io/compat-table/es6/#test-WeakMap
const firstNames = new WeakMap();
firstNames.set(this, 'Panda');
```

- 23.5 Don't save references to `this`. Use arrow functions or Function#bind

```js
// bad
function foo() {
  const self = this;
  return function () {
    console.log(self);
  };
}

// bad
function foo() {
  const that = this;
  return function () {
    console.log(that);
  };
}

// good
function foo() {
  return () => {
    console.log(this);
  };
}

```

- 23.6 A base filename should exactly match tyhe name of its default export.

```js
// file 1 contents
class CheckBox {
  // ...
}
export default CheckBox;

// file 2 contents
export default function fortyTwo() { return 42; }

// file 3 contents
export default function insideDirectory() {}

// in some other file
// bad
import CheckBox from './checkBox'; // PascalCase import/export, camelCase filename
import FortyTwo from './FortyTwo'; // PascalCase import/filename, camelCase export
import InsideDirectory from './InsideDirectory'; // PascalCase import/filename, camelCase export

// bad
import CheckBox from './check_box'; // PascalCase import/export, snake_case filename
import forty_two from './forty_two'; // snake_case import/filename, camelCase export
import inside_directory from './inside_directory'; // snake_case import, camelCase export
import index from './inside_directory/index'; // requiring the index file explicitly
import insideDirectory from './insideDirectory/index'; // requiring the index file explicitly

// good
import CheckBox from './CheckBox'; // PascalCase export/import/filename
import fortyTwo from './fortyTwo'; // camelCase export/import/filename
import insideDirectory from './insideDirectory'; // camelCase export/import/directory name/implicit "index"
// ^ supports both insideDirectory.js and insideDirectory/index.js

```

- 23.7 Use camelCase when you export-default a function. Your filename should be identical to your function's name.

```js
function makeStyleGuide() {

}

export default makeStyleGuide;

```

- 23.8 Use PascalCase when you export a constructor / class / singleton / function library / bare object.

```js

const Ajax = {
  post: {

  },
};

export default Ajax;

```

- 23.9 Acronyms and initialisms should always be capitalized or all lowercased.

> Names are for readability, not to appease a computer algorithm!

```js
// bad
import SmsContainer from './containers/SmsContainer';

// bad
const HttpRequests = [];

// good
import SMSContainer from './containers/SMSContainer';

// good
const HTTPRequests = [];

// also good
const httpRequests = [];

// best
import TextMessageContainer from './containers/TextMessageContainer';

// best
const requests = [];

```

- 23.10 You may optionally uppercase a constant only if it (1) is exported, (2) is a const (it can not be reassigned), and (3) the programmer can trust it (and its nested properties) to never change.

> This is an additonal tool to assit in situations where the programmer would be unsure if a variable might ever change. UPPERCASE_VARIABLES are letting the programmer know that they can trust the variable and any properties to not change.

- 
  - What about all `const` variables? - This is unnecessary, so uppercasing should not be used for constants within a file. It should be used for exported constants however.
  - What about exported objects? - Uppercase at the top level of export (e.g. `EXPORTED_OBJECT.key`) and maintain that all nested properties do not change.

```js
// bad
const PRIVATE_VARIABLE = 'should not be unnecessarily uppercased within a file';

// bad
export const THING_TO_BE_CHANGED = 'should obviously not be uppercased';

// bad
export let REASSIGNABLE_VARIABLE = 'do not use let with uppercase variables';

// ---

// allowed but does not supply semantic value
export const apiKey = 'SOMEKEY';

// better in most cases
export const API_KEY = 'SOMEKEY';

// ---

// bad - unnecessarily uppercases key while adding no semantic value
export const MAPPING = {
  KEY: 'value'
};

// good
export const MAPPING = {
  key: 'value'
};

```

## Accessors

- 24.1 Accesor functions for properties are not required.

- 24.2 Do not use JS getters/setters as they cause unexpected side effects and are harder to test, maintain, and reason about. Instead, if you do make accessor functions use `getVal()` and `setVal('hello')`

```js
// bad
class Dragon {
  get age() {
    // ...
  }

  set age(value) {
    // ...
  }
}

// good
class Dragon {
  getAge() {
    // ...
  }

  setAge(value) {
    // ...
  }
}
```

- 24.3 if the property/method is a `boolean`, use `isVal()` or `hasVal()`.

```js
// bad
if (!dragon.age()) {
  return false;
}

// good
if (!dragon.hasAge()) {
  return false;
}
```

- 24.4 It's okay to create `get()` and `set()` functions, but be consistent.

```js
class Jedi {
  constructor(options = {}) {
    const lightsaber = options.lightsaber || 'blue';
    this.set('lightsaber', lightsaber);
  }

  set(key, val) {
    this[key] = val;
  }

  get(key) {
    return this[key];
  }
}

```

## Events

- 25.1 When attaching data payloads to events (whether DOM events or something more proprietary like Backbone events), pass an object literal (also known as a "hash") instead of a raw value. This allows a subsequent contributor to add more data to the event payload without finding and updating every handler for the event. For example, instead of:

```js
// bad
$(this).trigger('listingUpdated', listing.id);

//...

$(this).on('listingUpdated', (e, listingID) => {
  // do a thing here
});
```

prefer:

```js
// good
$(this).trigger('listingUpdated', { listingID: listing.id });

// ..

$(this).on('listingUpdated', (e, data) => {
  // do something with data.listingID
});
```

## jQuery

Ideally don't use it but if you have too....

- 26.1 Prefix jQuery variables with a `$`,

```js
// bad
const sidebar = $('.sidebar');

// good
const $sidebar = $('.sidebar');
```

- 26.2 Cache jQuery lookups.

```js
// bad
function setSidebar() {
  $('.sidebar').hide();

  // ...

  $('.sidebar').css({
    'background-color': 'pink',
  });
}

// good
function setSidebar() {
  const $sidebar = $('.sidebar');
  $sidebar.hide();

  // ...

  $sidebar.css({
    'background-color': 'pink',
  });
}

```

- 26.3 For DOM queries use Cascading `$('.sidebar ul')` or parent > child `$('.sidebar > ul')`

> Performance reasons [See Here](https://jsperf.com/jquery-find-vs-context-sel/16)

- 26.4 Use `find` with scoped jQuery object queries.

```js
// bad
$('ul', '.sidebar').hide();

// bad
$('.sidebar').find('ul').hide();

// good
$('.sidebar ul').hide();

// good
$('.sidebar > ul').hide();

// good
$sidebar.find('ul').hide();
```

## ECMAScript 5 Compatibility

- 27.1 Refer to this [compatibility table](http://kangax.github.io/compat-table/es5/)

## Standard Library

The [Standard Library](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects) contains utilities that are functionally broken but remain for legacy reasons.

- 29.1 Use `Number.isNaN` instead of global `isNaN`. eslint: `no-restricted-globals`

> The global `isNaN` coerces non-numbers to numbers, returning true for anything that coerces to NaN.

```js
// bad
isNaN('1.2'); // false
isNaN('1.2.3'); // true

// good
Number.isNaN('1.2.3'); // false
Number.isNaN(Number('1.2.3')); // true
```

- 29.2 Use `Number.isFinite` instead of global `isFinite`. eslint: `no-restricted-globals`

> The global isFinite coerces non-numbers to numbers, returning true for anything that coerces to a finite number.

```js
// bad
isFinite('2e3'); // true

// good
Number.isFinite('2e3'); // false
Number.isFinite(parseInt('2e3', 10)); // true
```