module.exports = {
    title: 'Coding Standards',
    themeConfig: {
        sidebar: [
            ['/', 'Home'],
            ['/Javascript/', 'Javascript - Draft']
        ],
        lastUpdated: 'Last Updated'
    },
    port: 8085
}